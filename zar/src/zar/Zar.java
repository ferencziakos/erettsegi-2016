package zar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.exit;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feren
 */
public class Zar {

    public Zar(String[] szovegtomb) {
        tanulnivalo(szovegtomb);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Zar zar = new Zar(args);
        String[] ajtokodok = new String[500];
        int sorokszama = 0;
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("ajto.txt");
            Scanner scanner = new Scanner(inputStream);

            // Ez egy segédváltozó az indexeléshez.
            // 1. feladat
            System.out.println("1. feladat");
            sorokszama = 0;
            while (scanner.hasNextLine()) {
                String sor = scanner.nextLine();

                ajtokodok[sorokszama] = sor;

                sorokszama++;
            }
            inputStream.close();
        } catch (Exception ex) {
            Logger.getLogger(Zar.class.getName()).log(Level.SEVERE, null, ex);
            exit(1);
        }

        if (sorokszama == 0) {
            System.out.println("Nem volt adat a fileban.");
            exit(1);
        }

        // 2. feladat
        System.out.println("2. feladat");

        Scanner consolerolOlvaso = new Scanner(System.in);
        System.out.println("Adja meg a kódszámot:");

        String kodszam = consolerolOlvaso.nextLine();
        // todo: eltávolítani ezt:
        // String kodszam = "239451";

        System.out.println("3. feladat");

        for (int i = 0; i < sorokszama; i++) {
            if (ajtokodok[i].equals(kodszam)) {
                System.out.println((i + 1) + " ");
            }
        }

        System.out.println("4. feladat");

        boolean didWeFoundOne = false;
        for (int i = 0; i < sorokszama; i++) {
            String aktualisSor = ajtokodok[i];
            for (int j = 0; j < aktualisSor.length() - 1; j++) {
                for (int k = j + 1; k < aktualisSor.length(); k++) {
                    if (aktualisSor.charAt(j) == aktualisSor.charAt(k)) {
                        System.out.println(aktualisSor);
                        didWeFoundOne = true;
                        break;
                    }
                }

                if (didWeFoundOne) {
                    break;
                }
            }

            if (didWeFoundOne) {
                break;
            }
        }

        if (!didWeFoundOne) {
            System.out.println("Nem volt ismétlődő szó.");
        }

        // 5. feladat
        System.out.println("5. feladat");

        String kulcsszo = "";
        while (kodszam.length() <= 10 && kulcsszo.length() != kodszam.length()) {
//            Előltesztelőssel pl így lehet
//            boolean flag = true;
//            while (flag) {
//                int ezt = (int) (Math.random() * 10);                
//                boolean jabazmeg = false;
//                
//                for (int i = 0; i < kulcsszo.length(); i++) {
//                    if (random == Integer.valueOf(kulcsszo.charAt(i))) {
//                        jabazmeg = true;
//                    }
//                }
//                
//                flag = jabazmeg;
//            }

//                    char x = 'A';
//                    int xx = 65;
//                    
//                    System.out.println((int) 'A');
//                    
//                    if (x == xx) {
//                        System.out.println("FASZOM!");
//                    }
            boolean ismetlodik = false;
            do {
                int random = (int) (Math.random() * 10);
                ismetlodik = false;

                for (int i = 0; i < kulcsszo.length(); i++) {
                    if (random == Integer.valueOf(String.valueOf(kulcsszo.charAt(i)))) {
                        ismetlodik = true;
                    }
                }

                if (!ismetlodik) {
                    kulcsszo += String.valueOf(random);
                }

            } while (ismetlodik);
        }

        System.out.println("5. feladat");
        System.out.println(kulcsszo);

        // 6. feladat
        System.out.println("6. feladat");

        try {
            // 7. feladat
            // Truncate file.
            PrintWriter writer = null;

            writer = new PrintWriter("siker.txt");
            writer.print("");
            writer.flush();
            writer.close();

            writer = new PrintWriter("siker.txt");

            for (int i = 0; i < sorokszama; i++) {
                if (kodszam.length() != ajtokodok[i].length()) {
                    writer.println(ajtokodok[i] + " hibás hossz");
                    continue;
                }
                if (!nyit(ajtokodok[i], "340562")) {
                    writer.println(ajtokodok[i] + " hibás kódszám");
                    continue;
                }
                writer.println(ajtokodok[i] + " sikeres");
            }

            writer.flush();
            writer.close();
        } catch (Exception ex) {
            Logger.getLogger(Zar.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Nem tudtam megnyitni a kimeneti filet, he!");
            exit(1);
        }
    }

    public static boolean nyit(String jo, String proba) {
        // String teszt = true == true ? "eztutiigaz" : "ja, hat ez sose lesz";

        boolean egyezik = jo.length() == proba.length();
        if (egyezik) {
            int elteres = jo.charAt(0) - proba.charAt(0);
            for (int i = 1; i < jo.length(); i++) {
                if (elteres - (jo.charAt(i) - proba.charAt(i)) % 10 != 0) {
                    egyezik = false;
                }
            }
        }
        return egyezik;
    }

    public int addVisszaASzamomat() {
        int szam = 10;
        return szam;
    }

    public void tanulnivalo(String[] args) {
        System.out.println("Innen jon a for:");
        for (int i = 0; i < args.length; i++) {
            // System.out.println(x);
            // ["akos", "vagyok", "ferenczi"]
            System.out.println("args[" + i + "] --> " + args[i]);
        }

        String[] myArray = {};

        int[] entombom = {};

        int x = 5;

        myArray[0] = "string";
        entombom[0] = x;
    }

}
